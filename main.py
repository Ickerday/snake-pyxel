from collections import namedtuple, deque
from enum import unique, Enum
from typing import Optional

import pyxel

# DONE: Make snake object
# DONE: Make snake head sprite
# DONE: Make snake body sprite
# DONE: Make snake tail sprite
# TODO: Load snake sprite
# TODO: Make snake react to inputs
# TODO: Make snake move
# TODO: Make food sprite
# TODO: Make food object
# TODO: Make food spawn on tiles randomly
# TODO: Make snake eat food
# TODO: Make snake grow when eats food
# TODO: Make snake tail follow after head (recursion?)
# TODO: Make player lose when collides with self
# TODO: Make player lose when hits wall? (do I want walls?)


Point = namedtuple('Point', ['x', 'y'])


@unique
class Direction(Enum):
    UP = pyxel.KEY_UP
    DOWN = pyxel.KEY_DOWN
    LEFT = pyxel.KEY_LEFT
    RIGHT = pyxel.KEY_RIGHT


INITIAL_WIDTH: int = 150
INITIAL_HEIGHT: int = 150

FPS: int = 20
EXIT_KEY: int = pyxel.KEY_ESCAPE


class App:
    player_score: int = 0
    player_alive: bool = True
    player_direction: Direction = None
    player_sections: deque[Point] = deque([Point(x=round(INITIAL_HEIGHT / 2), y=round(INITIAL_WIDTH / 2))])

    def __init__(self) -> None:
        pyxel.init(width=INITIAL_WIDTH, height=INITIAL_HEIGHT, caption='Snake', fps=FPS, quit_key=EXIT_KEY)
        pyxel.mouse(True)

        pyxel.load('assets/snake.pyxres')

        pyxel.run(self.update, self.draw)

    def update(self) -> None:
        # TODO: If player ate a fruit
        # TODO: - Remove fruit sprite
        # TODO: - Add coords of fruit to top of snake list

        new_player_direction = self.resolve_input()
        if new_player_direction is not None:
            self.player_direction = new_player_direction

        # TODO: Get next player head position
        new_head_position = Point(0, 0)

        # TODO: Update player head position
        self.player_sections.appendleft(new_head_position)

        # TODO: Perchance `deque()` can do it byt itself by adjusting the maxlen (which in turn would be the number of fruit eaten)
        # Suggest movement of the snake by removing the last segment
        self.player_sections.pop()

        head: Point = self.player_sections[0]

        body: Optional[list[Point]] = self.player_sections[1:-1] if len(self.player_sections) > 2 else None

        tail: Optional[Point] = self.player_sections[-1] if len(self.player_sections) >= 2 else None

        # TODO: Draw head at first item of deque
        # FUCK: Blitting doesn't fucking work, the docs are lacking and tbh I'm utterly disappointed
        # FUCK: Event the blitting method is "blt" because we like being obtuse so much
        pyxel.blt(img=0,
                  u=0, v=0,
                  w=16, h=16,
                  x=head.x, y=head.y)

        # TODO: Draw body sections
        for pt in self.player_sections:
            pyxel.blt(img=0, colkey=0,
                      u=0, v=16,
                      w=16, h=16,
                      x=pt.x, y=pt.y)
            # x=10, y=10)

        # TODO: Draw body at middle items of deque

        # TODO: Draw tail at last item of deque

    def draw(self) -> None:
        pyxel.cls(0)

    @staticmethod
    def resolve_input() -> Optional[Direction]:
        for member in Direction:
            if pyxel.btn(member.value):
                return member

        return None


if __name__ == '__main__':
    App()
